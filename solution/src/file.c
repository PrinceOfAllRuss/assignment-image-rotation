#include "bmp.h"
#include "image.h"
#include <stdio.h>

struct bmp_header read_header(FILE* file) {
    struct bmp_header bmp;
    fread(&bmp, sizeof(struct bmp_header), 1, file);
    return bmp;
}

int read_pixels(FILE* file, const struct bmp_header* bmp, struct pixel* pixels, const long* indent) {
    struct pixel* pixels_pointer = pixels;
    for(size_t i = 0; i < bmp->biHeight; i++) {
        fread(pixels_pointer, sizeof(struct pixel), bmp->biWidth, file);
        fseek(file, *indent, SEEK_CUR);
        pixels_pointer += bmp->biWidth;
    }
    return 0;
}

int write_pixels_to_file(FILE* file, const struct bmp_header* bmp, struct pixel* pixels, const long* indent) {
    struct pixel* pixels_pointer = pixels;
    for(size_t i = 0; i < bmp->biHeight; i++) {
        fwrite(pixels_pointer, sizeof(struct pixel), bmp->biWidth, file);
        fwrite(indent, *indent, 1, file);
        pixels_pointer += bmp->biWidth;
    }
    return 0;
}

int write_to_file(FILE* file, const struct bmp_header* bmp, struct image* img, const long *indent) {
    fwrite(bmp, sizeof(struct bmp_header), 1, file);
    write_pixels_to_file(file, bmp, img->data, indent);
    return 0;
}
