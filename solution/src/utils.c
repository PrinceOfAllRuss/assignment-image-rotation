#include "bmp.h"
#include "image.h"

long count_indent(const struct bmp_header* bmp) {
    return (4l - ((long) (bmp->biWidth * sizeof(struct pixel)) % 4)) % 4;
}

struct image from_bmp_to_img(const struct bmp_header* bmp, struct pixel* pixels) {
    struct image img = {.width = bmp->biWidth, .height = bmp->biHeight, .data = pixels};
    return img;
}

struct bmp_header from_img_to_bmp(const struct bmp_header* bmp, struct image* img) {
    struct bmp_header new_bmp = *bmp;
    new_bmp.biWidth = img->width;
    new_bmp.biHeight = img->height;
    long new_indent = count_indent(&new_bmp);
    new_bmp.bfileSize = sizeof(struct bmp_header) +
                        (new_bmp.biWidth * sizeof(struct pixel) + new_indent) * new_bmp.biHeight;
    return new_bmp;
}

struct image rotate(struct image* img, struct pixel* new_pixels) {
    size_t index_for_new_pixels = 0;
    for(size_t i = 0; i < img->width; i++) {
        for(size_t j = img->height - 1; j != -1; j--) {
            new_pixels[index_for_new_pixels] = img->data[i + j * img->width];
            index_for_new_pixels++;
        }
    }
    struct image new_img = {.width = img->height, .height = img->width, .data = new_pixels};
    return new_img;
}
