#include "bmp.h"
#include "file.h"
#include "image.h"
#include "utils.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // suppress 'unused parameters' warning

    if (argc < 3) {
        fprintf(stderr, "Wrong input\n");
        return 1;
    }
    char* path_in = argv[1];
    char* path_out = argv[2];
    FILE* file;
    file = fopen(path_in, "rb");
    if(file == NULL) {
        fprintf(stderr, "Cant open file\n");
        return 0;
    }
    const struct bmp_header bmp = read_header(file);
    const uint16_t condition = check_bmp(&bmp);
    if(!condition) return 0;

    const long indent = count_indent(&bmp);
    struct pixel* pixels = malloc(sizeof(struct pixel) * bmp.biWidth * bmp.biHeight);
    if (pixels == NULL) return 1;
    read_pixels(file, &bmp, pixels, &indent);

    fclose(file);

    struct image img = from_bmp_to_img(&bmp, pixels);
    struct pixel* new_pixels = malloc(sizeof(struct pixel) * img.width * img.height);
    if (new_pixels == NULL) return 1;
    struct image new_img = rotate(&img, new_pixels);
    const struct bmp_header new_bmp = from_img_to_bmp(&bmp, &new_img);
    const long new_indent = count_indent(&new_bmp);
    FILE* file2;
    file2 = fopen(path_out, "wb");
    write_to_file(file2, &new_bmp, &new_img, &new_indent);
    free(pixels);
    free(new_pixels);
    printf("End of app");

    return 0;
}
