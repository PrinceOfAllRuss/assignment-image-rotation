#include <stdint.h>
#include <stdio.h>

const uint16_t BMP_TYPE = 0x4D42;
const uint16_t BMP_BIT_COUNT = 24;

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct pixel {
    uint8_t b, g, r;
};

int check_bmp(const struct bmp_header* bmp) {
    if(bmp->bfType != BMP_TYPE) {
        fprintf(stderr, "Wrong type of file\n");
        return 0;
    }
    if(bmp->biBitCount != BMP_BIT_COUNT) {
        fprintf(stderr, "Wrong type of file\n");
        return 0;
    }
    printf("All good\n");
    return 1;
}
