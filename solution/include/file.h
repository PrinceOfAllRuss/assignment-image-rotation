#ifndef FILE_H
#define FILE_H
#include "bmp.h"
#include "image.h"
#include <stdio.h>
struct bmp_header read_header(FILE* file);
int read_pixels(FILE* file, const struct bmp_header* bmp, struct pixel* pixels, const long* indent);
int write_pixels_to_file(FILE* file, const struct bmp_header* bmp, struct pixel* pixels, const long* indent);
int write_to_file(FILE* file, const struct bmp_header* bmp, struct image* img, const long *indent);
#endif
