#ifndef UTILS_H
#define UTILS_H
#include "bmp.h"
#include "image.h"
long count_indent(const struct bmp_header* bmp);
struct image from_bmp_to_img(const struct bmp_header* bmp, struct pixel* pixels);
struct bmp_header from_img_to_bmp(const struct bmp_header* bmp, struct image* img);
struct image rotate(struct image* img, struct pixel* new_pixels);
#endif
