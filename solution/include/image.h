#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#endif
